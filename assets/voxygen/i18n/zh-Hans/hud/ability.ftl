common-abilities-debug-possess = 附身箭矢
    .desc = 射出一支毒箭矢，使你能够控制你的目标.
common-abilities-hammer-leap = 毁灭之击
    .desc = 跳跃攻击指定位置,造成击退和范围伤害
common-abilities-bow-shotgun = 连发
    .desc = 连续发射一连串箭矢.
common-abilities-staff-fireshockwave = 火环术
    .desc = 猛砸地面造成圆圈范围的火焰冲击波.
common-abilities-sceptre-wardingaura = 守护光环
    .desc = 大幅提升盟友防御力,以抵御敌人攻击

# Sword abilities
veloren-core-pseudo_abilities-sword-heavy_stance = 重剑姿态
    .desc = 此姿态下的攻击可以使敌人晕眩,并对被晕眩的敌人造成更多伤害,但攻击速度较慢.
veloren-core-pseudo_abilities-sword-agile_stance = 轻剑姿态
    .desc = 此姿态下的攻击速度更快,但威力较低.
veloren-core-pseudo_abilities-sword-defensive_stance = 防守姿态
    .desc = 此姿态下的攻击可以稍微抵挡伤害或进行招架攻击.
veloren-core-pseudo_abilities-sword-crippling_stance = 致残姿态
    .desc = 此姿态下的攻击会造成或加重持续伤害效果.
veloren-core-pseudo_abilities-sword-cleaving_stance = 劈砍姿态
    .desc = 此姿态下的攻击可以同时命中多个敌人.
veloren-core-pseudo_abilities-sword-double_slash = 双连斩
    .desc = 连斩两次
common-abilities-sword-basic_double_slash = 基础双连斩
    .desc = 基础的连续斩击两次
common-abilities-sword-heavy_double_slash = 重型双连斩
    .desc = 较慢的两连斩,可以造成晕眩效果
common-abilities-sword-agile_double_slash = 快速双连斩
    .desc = 快速的进行两次连斩,但攻击伤害较低
common-abilities-sword-defensive_double_slash = 防守双连斩
    .desc = 连续连击两次,能够降低敌人攻击的影响
common-abilities-sword-crippling_double_slash = 致残双连斩
    .desc = 连续斩击两次,能够延长敌人的流血状态
common-abilities-sword-cleaving_double_slash = 劈砍连斩
    .desc = 连斩两次,能够同时攻击多名敌人
veloren-core-pseudo_abilities-sword-secondary_ability = 副剑技
    .desc = 绑定到副手攻击键的技能
common-abilities-sword-basic_thrust = 基础突进
    .desc = 蓄力攻击可以使突进伤害提升
common-abilities-sword-heavy_slam = 蓄力挥斩
    .desc = 威力巨大的向上挥斩,蓄力攻击的能够造成更强的眩晕效果
common-abilities-sword-agile_perforate = 钻墙打孔
    .desc = 迅速连续的轻攻击
common-abilities-sword-agile_dual_perforate = 双刺
    .desc = 迅速连续的双剑轻攻击
common-abilities-sword-defensive_vital_jab = 致命突刺
    .desc = 快速蓄力的前刺,对于被格挡的敌人造成更多的伤害
common-abilities-sword-crippling_deep_rend = 深度切割
    .desc = 专注于攻击已撕裂的伤害,对流血的敌人造成更多伤害
common-abilities-sword-cleaving_spiral_slash = 螺旋斩
    .desc = 将剑挥动一圈来攻击周围的敌人	
common-abilities-sword-cleaving_dual_spiral_slash = 双螺旋斩
    .desc = 将两把剑全程挥动以攻击周围的敌人
veloren-core-pseudo_abilities-sword-crescent_slash = 新月斩
    .desc =
        从下到上的斜斩
        根据攻击姿态进行改变
common-abilities-sword-basic_crescent_slash = 基础新月斩
    .desc = 一个基础的上斜斩
common-abilities-sword-heavy_crescent_slash = 重型新月斩
    .desc = 一个可以眩晕敌人的上斜斩
common-abilities-sword-agile_crescent_slash = 轻盈新月斩
    .desc = 一个轻盈的上斜斩
common-abilities-sword-defensive_crescent_slash = 防守新月斩
    .desc = 一个用于防守的上斜斩
common-abilities-sword-crippling_crescent_slash = 致残新月斩
    .desc = 一个可以使敌人流血的上斜斩
common-abilities-sword-cleaving_crescent_slash = 劈砍新月斩
    .desc = 一个可以穿透敌人的上斜斩
veloren-core-pseudo_abilities-sword-fell_strike = 坠击
    .desc =
        一次快速而强力的斩击
        根据攻击姿态改变
common-abilities-sword-basic_fell_strike = 基础落击
    .desc = 基础快速且强力的斩击
common-abilities-sword-heavy_fell_strike = 重型落击
    .desc = 一个可以眩晕敌人的强力斩击
common-abilities-sword-agile_fell_strike = 轻型落击
    .desc = 一个非常快速的强力斩击
common-abilities-sword-defensive_fell_strike = 防守型落击
    .desc = 一个用于防守的快速而强力的斩击
common-abilities-sword-crippling_fell_strike = 致残落击
    .desc = 一个可以使敌人流血的快速而强力的斩击
common-abilities-sword-cleaving_fell_strike = 劈砍落击
    .desc = 一个可以穿透敌人的快速而强力的斩击
veloren-core-pseudo_abilities-sword-skewer = 穿刺
    .desc =
        进行一次刺击
        根据攻击姿态改变
common-abilities-sword-basic_skewer = 基础穿刺
    .desc = 一个基础的刺击
common-abilities-sword-heavy_skewer = 重型穿刺
    .desc = 一个可以眩晕敌人的刺击
common-abilities-sword-agile_skewer = 轻型穿刺
    .desc = 一个快速的刺击
common-abilities-sword-defensive_skewer = 防守型穿刺
    .desc = 一个用于防守的刺击
common-abilities-sword-crippling_skewer = 致残穿刺
    .desc = 一个可以使敌人流血的刺击
common-abilities-sword-cleaving_skewer = 劈砍穿刺
    .desc = 一个可以穿透敌人的刺击
veloren-core-pseudo_abilities-sword-cascade = 瀑布
    .desc =
        一次上挥斩
        根据攻击姿态改变
common-abilities-sword-basic_cascade = 基础瀑布
    .desc = 一个基础的上挥斩
common-abilities-sword-heavy_cascade = 重型瀑布
    .desc = 一个可以眩晕敌人的上挥斩
common-abilities-sword-agile_cascade = 轻型瀑布
    .desc = 一个快速的上挥斩
common-abilities-sword-defensive_cascade = 防守瀑布
    .desc = 一个用于防守的上挥斩
common-abilities-sword-crippling_cascade = 致残瀑布
    .desc = 一个可以使敌人流血的上挥斩
common-abilities-sword-cleaving_cascade = 劈砍瀑布
    .desc = 一个可以穿透敌人的上挥斩
veloren-core-pseudo_abilities-sword-cross_cut = 十字斩
    .desc =
        进行X字形的斩击
        根据攻击姿态改变
common-abilities-sword-basic_cross_cut = 基础十字斩
    .desc = 一个基础的交叉斩击
common-abilities-sword-heavy_cross_cut = 重型十字斩
    .desc = 可以使敌人晕眩的交叉斩击
common-abilities-sword-agile_cross_cut = 轻型十字斩
    .desc = 快速的交叉斩击
common-abilities-sword-defensive_cross_cut = 防守十字斩
    .desc = 可以招架的交叉斩击
common-abilities-sword-crippling_cross_cut = 致残十字斩
    .desc = 可以造成流血效果的交叉斩击
common-abilities-sword-cleaving_cross_cut = 劈砍十字斩
    .desc = 可以贯穿敌人的交叉斩击
common-abilities-sword-basic_dual_cross_cut = 基础双重十字斩
    .desc = 同时进行的基础的交叉斩击
common-abilities-sword-heavy_dual_cross_cut = 重型双重十字斩
    .desc = 同时进行的可以使敌人晕眩的交叉斩击
common-abilities-sword-agile_dual_cross_cut = 轻型双重十字斩
    .desc = 同时进行快速的交叉斩击
common-abilities-sword-defensive_dual_cross_cut = 防守双重十字斩
    .desc = 同时进行交叉斩击的防守姿态
common-abilities-sword-crippling_dual_cross_cut = 致残双重十字斩
    .desc = 同时进行的可以造成流血效果的交叉斩击
common-abilities-sword-cleaving_dual_cross_cut = 劈砍双重十字斩
    .desc = 同时进行的可以贯穿敌人的左右斩击
veloren-core-pseudo_abilities-sword-finisher = 终结技
    .desc =
        一种消耗连击点数并用于结束战斗的能力
        终结技会根据你的战斗姿态不同而有所不同
common-abilities-sword-basic_mighty_strike = 强力打击
    .desc =
        一个简单但威力强大的斩击
        使用时需要消耗部分连击点数
common-abilities-sword-heavy_guillotine = 斩首
    .desc =
        一个强力的劈砍,可能会使敌人晕眩或击杀
        使用时需要消耗部分连击点数
common-abilities-sword-agile_hundred_cuts = 百刃乱舞
    .desc =
        对目标进行多次极快的连续斩击
        使用时需要消耗部分连击点数
common-abilities-sword-defensive_counter = 反击
    .desc =
        快速发动的攻击,对被格挡的敌人造成更多伤害
        使用时需要消耗部分连击点数
common-abilities-sword-crippling_mutilate = 肢解
    .desc =
        通过切割敌人的伤口来造成更多伤害,对流血状态的敌人效果更佳
        使用时需要消耗部分连击点数
common-abilities-sword-cleaving_bladestorm = 剑刃风暴
    .desc =
        用剑进行多次旋转挥击,摧毁你的敌人
        使用时需要消耗部分连击点数
common-abilities-sword-cleaving_dual_bladestorm = 剑刃风暴II
    .desc =
        使用两把剑进行多次旋转攻击,摧毁你的敌人
        使用时需要消耗部分连击点数
common-abilities-sword-heavy_sweep = 重型横扫
    .desc =
        进行一次强力横扫,对被晕眩的敌人造成更多伤害
        进入重型姿态
common-abilities-sword-heavy_pommel_strike = 钝击
    .desc =
        用钝击打击敌人的头部,造成眩晕
        进入重型姿态
common-abilities-sword-agile_quick_draw = 拔剑术
    .desc =
        快速冲刺并同时抽出你的剑进行攻击
        进入轻型姿态
common-abilities-sword-agile_feint = 佯攻
    .desc =
        向一侧闪避然后返回并发动攻击
        进入轻型姿态
common-abilities-sword-defensive_riposte = 还击
    .desc =
        格挡敌人的攻击后立即反击
        进入防守姿态
common-abilities-sword-defensive_disengage = 后撤步
    .desc =
        攻击后快速后退一段距离
        进入防守姿态
common-abilities-sword-crippling_gouge = 撕裂
    .desc =
        对敌人造成伤害并造成持续流血的伤口
        进入致残姿态
common-abilities-sword-crippling_hamstring = 当筋立断
    .desc =
        对敌人的经脉造成伤害,使其机动性降低
        进入致残姿态
common-abilities-sword-cleaving_whirlwind_slice = 旋风斩
    .desc =
        螺旋式的攻击你周围的敌人
        进入劈砍姿态
common-abilities-sword-cleaving_dual_whirlwind_slice = 双重旋风斩
    .desc =
        使用两把剑进行旋转攻击周围所有敌人
        进入劈砍姿态
common-abilities-sword-cleaving_earth_splitter = 裂地斩
    .desc =
        劈开大地,如果在下落时使用对目标的伤害提升更大
        进入劈砍姿态
common-abilities-sword-heavy_fortitude = 刚毅
    .desc =
        增强抗晕能力,当受到伤害时,你造成的伤害将会更加容易眩晕敌人
        需要重型姿态
common-abilities-sword-heavy_pillar_thrust = 镇压
    .desc =
        用剑刺向敌人,一直刺到地面上,如果在下落时使用,威力更大
        需要重型姿态
common-abilities-sword-agile_dancing_edge = 舞动之刃
    .desc =
        移动和攻击更迅速
        需要轻型姿态
common-abilities-sword-agile_flurry = 乱劈
    .desc =
        进行数次快速突刺
        需要轻型姿态
common-abilities-sword-agile_dual_flurry = 乱劈II
    .desc =
        使用双剑进行多次快速刺击
        需要轻型姿态
common-abilities-sword-defensive_stalwart_sword = 坚定之剑
    .desc =
        抵挡大部分攻击,减少受到的伤害
        需要防守姿态
common-abilities-sword-defensive_deflect = 偏转
    .desc =
        极快的取剑格挡前方,快到甚至能够抵挡弹道
        需要防守姿态
common-abilities-sword-crippling_eviscerate = 毁伤
    .desc =
        进一步撕裂创伤,对受伤的敌人造成更多伤害
        需要致残姿态
common-abilities-sword-crippling_bloody_gash = 流血重创
    .desc =
        残忍地攻击已在流血的伤口上,对流血的敌人造成更多伤害
        需要致残姿态
common-abilities-sword-cleaving_blade_fever = 狂热刀刃
    .desc =
        更加鲁莽地攻击,提升你的攻击力,但会降低防御力
        需要劈砍姿态
common-abilities-sword-cleaving_sky_splitter = 天地分裂
    .desc =
        据说能够劈开天空的强力斩击,但用来劈开敌人
        需要劈砍姿态

# Axe abilities
common-abilities-axe-triple_chop = 三连劈
    .desc =
        快速进行三次打击
common-abilities-axe-cleave = 分割
    .desc =
        向下砍击，可以积攒多段连击
common-abilities-axe-brutal_swing = 残暴挥击
    .desc =
        旋转挥动斧子,对附近敌人造成伤害
common-abilities-axe-berserk = 狂暴
    .desc =
        提高你的伤害,但会降低你的防御能力
common-abilities-axe-rising_tide = 涨潮
    .desc =
        向上挥击,大幅度增加连击
common-abilities-axe-savage_sense = 野性感知
    .desc =
        辨别目标的要害,确保你的下一次攻击造成暴击伤害
common-abilities-axe-adrenaline_rush = 肾上腺素爆发
    .desc =
        消耗所有连击来恢复体力
        激活时与连击相关,消耗所有连击
common-abilities-axe-execute = 处决
    .desc =
        一次毁灭性的攻击,非常致命
        需要30连击才能使用
        如果解锁了,50连击会自动升级为旋风
common-abilities-axe-maelstrom = 旋风
    .desc =
        用一次毁灭性的旋转攻击周围的一切
        从处决达到50连击时自动解锁
common-abilities-axe-rake = 裂伤
    .desc =
        用斧头在敌人身上划出伤口,造成流血效果
common-abilities-axe-bloodfeast = 血宴
    .desc =
        你的斧头渴望敌人的鲜血,每次攻击流血的敌人都会恢复你的生命值
common-abilities-axe-fierce_raze = 凶猛劈砍
    .desc =
        迅速连续地攻击你的敌人
common-abilities-axe-dual_fierce_raze = 凶猛劈砍II
    .desc =
        使用两把斧头卡快速连续攻击你的敌人
common-abilities-axe-furor = 狂怒
    .desc =
        进入狂怒,攻击将会获得更多连击次数
common-abilities-axe-fracture = 破碎
    .desc =
        一次重创,降低敌人的移动速度
        激活时与连击相关,消耗一半的连击
common-abilities-axe-lacerate = 撕裂
    .desc =
        剥皮攻击你的目标,造成流血伤害
        需要30连击才能使用
        如果达成条件,50连击时会自动升级为撕裂狂潮
common-abilities-axe-riptide = 撕裂狂潮
    .desc =
        撕裂周围的一切,让他们失去鲜血
        50连击时自动从撕裂升级
common-abilities-axe-skull_bash = 斧砸
    .desc =
        用斧头的平面攻击,可能会使敌人晕眩
common-abilities-axe-sunder = 破甲
    .desc =
        改变握持方式,你的攻击可以突破敌人的防御并为你恢复更多耐力.
common-abilities-axe-plunder = 掠夺
    .desc =
        快速靠近你的敌人,并攻击一次摧毁他们的平衡
common-abilities-axe-defiance = 背水一战
    .desc =
        直面死神,使自己对晕眩和死亡抗性提升
common-abilities-axe-keelhaul = 拖拽
    .desc =
        钩住敌人并将他们拉向你
        激活时与连击相关,消耗一半的连击
common-abilities-axe-bulkhead = 泰坦一击
    .desc =
        一次沉重的挥击,有些人说甚至能使泰坦晕眩
        需要30连击才能使用
        50连击会自动升级为倾覆
common-abilities-axe-capsize = 倾覆
    .desc =
        用一次沉重的旋转攻击让周围的一切晕眩
        达到50连击自动从泰坦一击升级
