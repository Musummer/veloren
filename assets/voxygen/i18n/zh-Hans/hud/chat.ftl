## Player events
hud-chat-online_msg = [{ $name }] 上线了.
hud-chat-offline_msg = [{ $name }] 下线了.

## Buff outcomes
hud-outcome-burning = 死于:燃烧
hud-outcome-curse = 死于:诅咒
hud-outcome-bleeding = 死于:流血
hud-outcome-crippled = 死于:残废
hud-outcome-frozen = 死于:冰冻
hud-outcome-mysterious = 死于:秘密(不明)

## Buff deaths
hud-chat-died_of_pvp_buff_msg = [{ $victim }] 死于 [{ $attacker }] 造成的 { $died_of_buff }
hud-chat-died_of_buff_nonexistent_msg = [{ $victim } ]死于 { $died_of_buff }
hud-chat-died_of_npc_buff_msg = [{ $victim }] 死于 { $attacker } 造成的 { $died_of_buff }

## PvP deaths
hud-chat-pvp_melee_kill_msg = [{ $attacker }] 击败了[{ $victim }]
hud-chat-pvp_ranged_kill_msg = [{ $attacker }] 射杀了[{ $victim }]
hud-chat-pvp_explosion_kill_msg = [{ $attacker }] 炸死了[{ $victim }]
hud-chat-pvp_energy_kill_msg = [{ $attacker }] 用魔法杀死了[{ $victim }]
hud-chat-pvp_other_kill_msg = [{ $attacker }] 杀死了[{ $victim }]

## PvE deaths
hud-chat-npc_melee_kill_msg = { $attacker } 杀死了[{ $victim }]
hud-chat-npc_ranged_kill_msg = { $attacker } 射杀了[{ $victim }]
hud-chat-npc_explosion_kill_msg = { $attacker } 炸死了[{ $victim }]
hud-chat-npc_energy_kill_msg = { $attacker } 用魔法杀死了[{ $victim }]
hud-chat-npc_other_kill_msg = { $attacker } 杀死了[{ $victim }]

## Other deaths
hud-chat-environmental_kill_msg = [{ $name }] 在 { $environment } 中死亡
hud-chat-fall_kill_msg = [{ $name }] 因坠落死亡
hud-chat-suicide_msg = [{ $name }] 自残致死
hud-chat-default_death_msg = [{ $name }] 死亡

## Utils
hud-chat-all = 全部
hud-chat-you = 你
hud-chat-chat_tab_hover_tooltip = 右键点击进行设置
hud-loot-pickup-msg = {$actor} 捡起了 { $amount ->
[one] { $item }
*[other] { $amount }x{ $item }
}
hud-chat-loot_fail = 你的背包已满！
hud-chat-goodbye = 再见！
hud-chat-connection_lost = 连接丢失。将在 { $time } 秒后断开连接。
