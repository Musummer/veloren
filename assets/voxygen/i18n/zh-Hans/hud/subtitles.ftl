subtitle-campfire = 篝火燃烧声
subtitle-bird_call = 鸟儿歌唱声
subtitle-bees = 蜜蜂嗡嗡声
subtitle-owl = 猫头鹰呼喊声
subtitle-running_water = 水流声
subtitle-lightning = 雷声

subtitle-footsteps_grass = 踩在草地上的脚步声
subtitle-footsteps_earth = 踩在泥土上的脚步声
subtitle-footsteps_rock = 踩在石头上的脚步声
subtitle-footsteps_snow = 踩在雪地上的脚步声
subtitle-pickup_item = 捡起物品声
subtitle-pickup_failed = 捡起失败声

subtitle-glider_open = 打开滑翔翼声音
subtitle-glider_close = 关闭滑翔翼声音
subtitle-glide = 滑翔声
subtitle-roll = 翻滚声
subtitle-swim = 游泳声
subtitle-climb = 攀爬声
subtitle-damage = 受伤声
subtitle-death = 死亡声

subtitle-wield_bow = 装备弓声音
subtitle-unwield_bow = 卸下弓声音
subtitle-pickup_bow = 捡起弓声音

subtitle-wield_sword = 装备剑声音
subtitle-unwield_sword = 卸下剑声音
subtitle-sword_attack = 挥剑声音
subtitle-pickup_sword = 捡起剑声音

subtitle-wield_axe = 装备斧头声音
subtitle-unwield_axe = 卸下斧头声音
subtitle-axe_attack = 挥斧头声音
subtitle-pickup_axe = 捡起斧头声音

subtitle-wield_hammer = 装备锤子声音
subtitle-unwield_hammer = 卸下锤子声音
subtitle-hammer_attack = 挥锤子声音
subtitle-pickup_hammer = 捡起锤子声音

subtitle-wield_staff = 装备法杖声音
subtitle-unwield_staff = 卸下法杖声音
subtitle-fire_shot = 法杖发射声音
subtitle-staff_attack = 法杖攻击声音
subtitle-pickup_staff = 捡起法杖声音

subtitle-wield_sceptre = 装备节杖声音
subtitle-unwield_sceptre = 卸下节杖声音
subtitle-sceptre_heal = 节杖治疗声音
subtitle-pickup_sceptre = 捡起节杖声音

subtitle-wield_dagger = 装备匕首声音
subtitle-uwield_dagger = 卸下匕首声音
subtitle-dagger_attack = 挥舞匕首声音
subtitle-pickup_dagger = 捡起匕首声音

subtitle-wield_shield = 装备盾牌声音
subtitle-unwield_shield = 卸下盾牌声音
subtitle-shield_attack = 盾牌推挤声音
subtitle-pickup_shield = 捡起盾牌声音

subtitle-pickup_pick = 捡起镐子声音
subtitle-pickup_gemstone = 捡起宝石声音

subtitle-instrument_organ = 风琴演奏声音

subtitle-wield_instrument = 装备乐器声音
subtitle-unwield_instrument = 卸下乐器声音
subtitle-instrument_double_bass = 演奏低音提琴声音
subtitle-instrument_flute = 演奏长笛声音
subtitle-instrument_glass_flute = 演奏玻璃长笛声音
subtitle-instrument_lyre = 演奏竖琴声音
subtitle-instrument_icy_talharpa = 演奏冰冷的塔尔哈帕琴声音
subtitle-instrument_kalimba = 演奏卡林巴琴声音
subtitle-instrument_melodica = 演奏电子口琴声音
subtitle-instrument_lute = 演奏吉他琴声音
subtitle-instrument_sitar = 演奏西塔琴声音
subtitle-instrument_guitar = 演奏吉他琴声音
subtitle-instrument_dark_guitar = 演奏黑暗吉他琴声音
subtitle-instrument_washboard = 演奏洗衣板声音
subtitle-instrument_wildskin_drum = 演奏野兽皮鼓声音
subtitle-pickup_instrument = 捡起乐器声音

subtitle-explosion = 爆炸声音

subtitle-arrow_shot = 箭矢发射声音
subtitle-arrow_miss = 箭矢未命中声音
subtitle-arrow_hit = 箭矢命中声音
subtitle-skill_point = 获得技能点声音
subtitle-sceptre_beam = 节杖光束声音
subtitle-flame_thrower = 喷火器声音
subtitle-break_block = 方块破坏声音
subtitle-attack_blocked = 攻击被格挡声音
subtitle-parry = 格挡声音
subtitle-interrupted = 中断声音
subtitle-stunned = 眩晕声音
subtitle-dazed = 迷茫声音
subtitle-knocked_down = 倒地声音

subtitle-attack-ground_slam = 地面猛击声音
subtitle-attack-laser_beam = 激光束声音
subtitle-attack-cyclops_charge = 独眼巨人冲锋声音
subtitle-giga_roar = 冰霜巨魔咆哮声音
subtitle-deep_laugh = 低沉笑声
subtitle-attack-flash_freeze = 闪电冻结声音
subtitle-attack-icy_spikes = 冰刺声音
subtitle-attack-ice_crack = 冰裂声音
subtitle-attack-steam = 蒸汽声音
subtitle-attack-shovel = 铲子挖掘声音

subtitle-consume_potion = 喝药水声音
subtitle-consume_apple = 吃苹果声音
subtitle-consume_cheese = 吃奶酪声音
subtitle-consume_food = 吃东西声音
subtitle-consume_liquid = 喝液体声音

subtitle-utterance-alligator-angry = 狗头人愤怒声音
subtitle-utterance-antelope-angry = 羚羊愤怒声音
subtitle-utterance-biped_large-angry = 大型双足类生物愤怒声音
subtitle-utterance-bird-angry = 鸟类愤怒声音
subtitle-utterance-adlet-angry = 狼人愤怒声音
subtitle-utterance-pig-angry = 猪愤怒声音
subtitle-utterance-reptile-angry = 爬行动物愤怒声音
subtitle-utterance-sea_crocodile-angry = 海鳄愤怒声音
subtitle-utterance-saurok-angry = 蜥蜴人愤怒声音
subtitle-utterance-cat-calm = 猫咪喵喵叫声音
subtitle-utterance-cow-calm = 牛哞哞叫声音
subtitle-utterance-fungome-calm = 眼球菌怪安静声音
subtitle-utterance-goat-calm = 山羊安静声音
subtitle-utterance-pig-calm = 猪安静声音
subtitle-utterance-sheep-calm = 羊安静声音
subtitle-utterance-truffler-calm = 小猪怪安静声音
subtitle-utterance-human-greeting = 问候声音
subtitle-utterance-adlet-hurt = 狼人受伤声音
subtitle-utterance-antelope-hurt = 羚羊受伤声音
subtitle-utterance-biped_large-hurt = 大型双足类生物受伤声音
subtitle-utterance-human-hurt = 人类受伤声音
subtitle-utterance-lion-hurt = 狮子受伤声音
subtitle-utterance-mandroga-hurt = 曼陀罗蛇受伤声音
subtitle-utterance-maneater-hurt = 人吃怪受伤声音
subtitle-utterance-marlin-hurt = 旗鱼受伤声音
subtitle-utterance-mindflayer-hurt = 脑食人受伤声音
subtitle-utterance-dagon-hurt = 邪神受伤声音
subtitle-utterance-asp-angry = 蝰蛇愤怒声音
subtitle-utterance-asp-calm = 蝰蛇安静声音
subtitle-utterance-asp-hurt = 蝰蛇受伤声音
subtitle-utterance-wendigo-angry = 温迪戈愤怒声音
subtitle-utterance-wendigo-calm = 温迪戈安静声音
subtitle-utterance-wolf-angry = 狼愤怒声音
subtitle-utterance-wolf-hurt = 狼受伤声音
subtitle-utterance-wyvern-angry = 飞龙愤怒声音
subtitle-utterance-wyvern-hurt = 飞龙受伤声音